﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyBizAPMS
{
    public partial class FrmTrimsProcess : Form
    {
        public FrmTrimsProcess()
        {
            InitializeComponent();
            this.SfdDataGridTrims.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.SfdDataGridTrims.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        int Fillid = 0; int SelectId = 0;
        private int LoadId = 0;
        SqlConnection sqlconnection = new SqlConnection(GeneralParameters.ConnectionString);
        BindingSource bsDocNo = new BindingSource();
        BindingSource bsTrimsProcess = new BindingSource();
        SQLDBHelper db = new SQLDBHelper();
        DataTable dtTrims = new DataTable();
        DataTable dtUom = new DataTable();

        private void TxtOrderNo_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                string Query = "Select Uid,DocNo,DocDate,StyleDesc from OrderM Where Uid Not in (Select OrderMuid from OrderMBudjetTrimsProcess) and DocSts = 'Entry Completed' order by Uid desc";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, sqlconnection);
                bsDocNo.DataSource = dt;
                FillGrid(dt, 1);
                Point loc = Genclass.FindLocation(txtOrderNo);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 1)
                {
                    Fillid = 1;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "DocNo";
                    DataGridCommon.Columns[1].HeaderText = "DocNo";
                    DataGridCommon.Columns[1].DataPropertyName = "DocNo";
                    DataGridCommon.Columns[1].Width = 100;
                    DataGridCommon.Columns[2].Name = "StyleDesc";
                    DataGridCommon.Columns[2].HeaderText = "StyleDesc";
                    DataGridCommon.Columns[2].DataPropertyName = "StyleDesc";
                    DataGridCommon.Columns[2].Width = 210;
                    DataGridCommon.DataSource = bsDocNo;
                }
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "GUid";
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.Columns[0].Width = 0;
                    DataGridCommon.Columns[1].Name = "ProcessName";
                    DataGridCommon.Columns[1].HeaderText = "Process Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[1].Width = 200;
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ShortName";
                    DataGridCommon.DataSource = bsTrimsProcess;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetGeneralData(int TypemUid, int Active)
        {
            DataTable data = new DataTable();
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@TypeMUid", TypemUid), new SqlParameter("@Active", Active) };
                data = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetGeneralM", sqlParameters, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return data;
        }

        private void TxtProcess_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable data = GetGeneralData(32, 1);
                bsTrimsProcess.DataSource = data;
                FillGrid(data, 2);
                Point loc = Genclass.FindLocation(txtProcess);
                grSearch.Location = new Point(loc.X - 10, loc.Y + 20);
                grSearch.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 1)
                {
                    SelectId = 1;
                    txtOrderNo.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtOrderNo.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    DataTable dtq = GetorderStyle(Convert.ToDecimal(txtOrderNo.Tag));
                    CmbStyle.DisplayMember = "StyleName";
                    CmbStyle.ValueMember = "Uid";
                    CmbStyle.DataSource = dtq;
                    SelectId = 0;
                    CmbStyle_SelectedIndexChanged(sender, e);
                }
                else if (Fillid == 2)
                {
                    txtProcess.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtProcess.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataTable GetorderStyle(decimal orderId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@OrderMUid", orderId) };
                dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetOrderMStyles", parameters, sqlconnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dataTable;
        }

        private void CmbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0 && CmbStyle.SelectedIndex != -1)
                {
                    SelectId = 1;
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrderMUid", txtOrderNo.Tag) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetTrimsForTrimsProcess", sqlParameters, sqlconnection);
                    dtUom = dataTable;
                    CmbItem.DataSource = null;
                    CmbItem.DisplayMember = "ItemSpec";
                    CmbItem.ValueMember = "Uid";
                    CmbItem.DataSource = dataTable;
                    dtTrims = dataTable;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmTrimsProcess_Load(object sender, EventArgs e)
        {
            GrFront.Visible = true;
            GrBack.Visible = false;
            SqlParameter[] sqlParameters = {
                new SqlParameter("@TypeMUid", 7),
                new SqlParameter("@Active", 1),
            };
            DataTable dtUom = db.GetDataWithParam(CommandType.StoredProcedure, "Sp_GetGeneralM", sqlParameters, sqlconnection);
            CmbUom.DisplayMember = "GeneralName";
            CmbUom.ValueMember = "Guid";
            CmbUom.DataSource = dtUom;
            LoadDataGridTrims();
            LoadTrimsGrid();
        }

        private void SplitAdd_Click(object sender, EventArgs e)
        {
            try
            {
                GrFront.Visible = false;
                GrBack.Visible = true;
                txtDocNo.Text = GeneralParameters.GetDocNo(5, sqlconnection);
                txtDocNo.Tag = "0";
                CmbStyle.DataSource = null;
                txtDocNo.Text = string.Empty;
                txtOrderNo.Text = string.Empty;
                DataGridTrimsAdd.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0 && CmbItem.SelectedIndex != -1)
                {
                    DataTable dt = dtTrims.Select("Uid=" + CmbItem.SelectedValue + "").CopyToDataTable();
                    txtQty.Text = dt.Rows[0]["ReqQty"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                BtnSelect_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem.Text == "Back")
                {
                    GrFront.Visible = true;
                    GrBack.Visible = false;
                    LoadTrimsGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDataGridTrims()
        {
            DataGridTrimsAdd.DataSource = null;
            DataGridTrimsAdd.AutoGenerateColumns = false;
            DataGridTrimsAdd.ColumnCount = 10;

            DataGridTrimsAdd.Columns[0].Name = "ItemName";
            DataGridTrimsAdd.Columns[0].HeaderText = "Item";
            DataGridTrimsAdd.Columns[0].HeaderText = "Item";
            DataGridTrimsAdd.Columns[0].Width = 350;

            DataGridTrimsAdd.Columns[1].Name = "ProcessName";
            DataGridTrimsAdd.Columns[1].HeaderText = "Process";
            DataGridTrimsAdd.Columns[1].Width = 200;

            DataGridTrimsAdd.Columns[2].Name = "Colour";
            DataGridTrimsAdd.Columns[2].HeaderText = "Colour";

            DataGridTrimsAdd.Columns[3].Name = "UOM";
            DataGridTrimsAdd.Columns[3].HeaderText = "UOM";
            DataGridTrimsAdd.Columns[3].Width = 90;

            DataGridTrimsAdd.Columns[4].Name = "Qty";
            DataGridTrimsAdd.Columns[4].HeaderText = "Qty";
            DataGridTrimsAdd.Columns[4].Width = 80;

            DataGridTrimsAdd.Columns[5].Name = "Rate";
            DataGridTrimsAdd.Columns[5].HeaderText = "Rate";
            DataGridTrimsAdd.Columns[5].Width = 80;

            DataGridTrimsAdd.Columns[6].Name = "ProcessLoss";
            DataGridTrimsAdd.Columns[6].HeaderText = "ProcessLoss";
            DataGridTrimsAdd.Columns[6].Width = 75;

            DataGridTrimsAdd.Columns[7].Name = "ItemUid";
            DataGridTrimsAdd.Columns[7].HeaderText = "ItemUid";
            DataGridTrimsAdd.Columns[7].Visible = false;

            DataGridTrimsAdd.Columns[8].Name = "ProcessId";
            DataGridTrimsAdd.Columns[8].HeaderText = "ProcessId";
            DataGridTrimsAdd.Columns[8].Visible = false;

            DataGridTrimsAdd.Columns[9].Name = "Uid";
            DataGridTrimsAdd.Columns[9].HeaderText = "Uid";
            DataGridTrimsAdd.Columns[9].Visible = false;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbItem.SelectedIndex != -1 && txtProcess.Text != string.Empty && txtQty.Text != string.Empty && txtRate.Text != string.Empty)
                {
                    if (txtProcessLoss.Text == string.Empty)
                    {
                        txtProcessLoss.Text = "0";
                    }
                    if(txtRate.Text == string.Empty)
                    {
                        txtRate.Text = "0";
                    }
                    int Index = DataGridTrimsAdd.Rows.Add();
                    DataGridViewRow dataGridViewRow = DataGridTrimsAdd.Rows[Index];
                    dataGridViewRow.Cells[0].Value = CmbItem.Text;
                    dataGridViewRow.Cells[1].Value = txtProcess.Text;
                    dataGridViewRow.Cells[2].Value = txtColour.Text;
                    dataGridViewRow.Cells[3].Value = CmbUom.Text;
                    dataGridViewRow.Cells[4].Value = txtQty.Text;
                    dataGridViewRow.Cells[5].Value = txtRate.Text;
                    dataGridViewRow.Cells[6].Value = txtProcessLoss.Text;
                    dataGridViewRow.Cells[7].Value = CmbItem.SelectedValue;
                    dataGridViewRow.Cells[8].Value = txtProcess.Tag;
                    dataGridViewRow.Cells[9].Value = "0";

                    CmbItem.SelectedIndex = -1;
                    txtProcess.Text = string.Empty;
                    txtProcess.Tag = string.Empty;
                    txtColour.Text = string.Empty;
                    txtQty.Text = string.Empty;
                    txtRate.Text = string.Empty;
                    txtProcessLoss.Text = string.Empty;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < DataGridTrimsAdd.Rows.Count; i++)
                {
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@Uid",DataGridTrimsAdd.Rows[i].Cells[9].Value.ToString()),
                        new SqlParameter("@OrderMuid",txtOrderNo.Tag),
                        new SqlParameter("@OrderMStyleUid",CmbStyle.SelectedValue),
                        new SqlParameter("@TrimsItemUid",DataGridTrimsAdd.Rows[i].Cells[7].Value.ToString()),
                        new SqlParameter("@TrimsProcessUid",DataGridTrimsAdd.Rows[i].Cells[8].Value.ToString()),
                        new SqlParameter("@Colour",DataGridTrimsAdd.Rows[i].Cells[2].Value.ToString()),
                        new SqlParameter("@ProcessLoss",DataGridTrimsAdd.Rows[i].Cells[6].Value.ToString()),
                        new SqlParameter("@Qty",DataGridTrimsAdd.Rows[i].Cells[4].Value.ToString()),
                        new SqlParameter("@Rate",DataGridTrimsAdd.Rows[i].Cells[5].Value.ToString()),
                        new SqlParameter("@CreaetDate",DateTime.Now),
                        new SqlParameter("@UserId",GeneralParameters.UserdId),
                        new SqlParameter("@DocNo",txtDocNo.Text),
                        new SqlParameter("@UOM",DataGridTrimsAdd.Rows[i].Cells[3].Value.ToString()),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OrderMTrimsProcess", sqlParameters, sqlconnection);
                }
                if (txtDocNo.Tag.ToString() == "0")
                {
                    string Query = "Update DocTypeM Set Lastno = Lastno+1 Where DocTypeId = 5";
                    db.ExecuteNonQuery(CommandType.Text, Query, sqlconnection);
                }
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CmbStyle.DataSource = null;
                txtDocNo.Text = string.Empty;
                txtOrderNo.Text = string.Empty;
                DataGridTrimsAdd.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadTrimsGrid()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetOrderMTrimsProcess", sqlconnection);
                SfdDataGridTrims.DataSource = null;
                SfdDataGridTrims.DataSource = dataTable;
                SfdDataGridTrims.Columns[0].Width = 60;
                SfdDataGridTrims.Columns[2].Width = 240;
                SfdDataGridTrims.Columns[6].Visible = false;
                SfdDataGridTrims.Columns[5].Width = 390;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SplitAdd_DropDowItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if(e.ClickedItem.Text == "Close")
                {
                    this.Close();
                }
                else
                {
                    SelectId = 1;
                    if (SfdDataGridTrims.SelectedIndex == -1)
                    {
                        MessageBox.Show("Select a row", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    var selectedItem = SfdDataGridTrims.SelectedItems[0];
                    var dataRow = (selectedItem as DataRowView).Row;
                    decimal CUid = Convert.ToDecimal(dataRow["OrderMuid"].ToString());
                    SqlParameter[] sqlParameters = { new SqlParameter("@OrdermUid", CUid) };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_OrderMTrimsProcessEdit", sqlParameters, sqlconnection);
                    DataTable dtq = GetorderStyle(CUid);
                    CmbStyle.DisplayMember = "StyleName";
                    CmbStyle.ValueMember = "Uid";
                    CmbStyle.DataSource = dtq;
                    DataGridTrimsAdd.Rows.Clear();
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = DataGridTrimsAdd.Rows.Add();
                        DataGridViewRow dataGridViewRow = DataGridTrimsAdd.Rows[Index];
                        dataGridViewRow.Cells[0].Value = dataTable.Rows[i]["ItemSpec"].ToString();
                        dataGridViewRow.Cells[1].Value = dataTable.Rows[i]["process"].ToString();
                        dataGridViewRow.Cells[2].Value = dataTable.Rows[i]["Colour"].ToString();
                        dataGridViewRow.Cells[3].Value = dataTable.Rows[i]["UOM"].ToString();
                        dataGridViewRow.Cells[4].Value = dataTable.Rows[i]["Qty"].ToString();
                        dataGridViewRow.Cells[5].Value = dataTable.Rows[i]["Rate"].ToString();
                        dataGridViewRow.Cells[6].Value = dataTable.Rows[i]["ProcessLoss"].ToString();
                        dataGridViewRow.Cells[7].Value = dataTable.Rows[i]["TrimsItemUid"].ToString();
                        dataGridViewRow.Cells[8].Value = dataTable.Rows[i]["TrimsProcessUid"].ToString();
                        dataGridViewRow.Cells[9].Value = dataTable.Rows[i]["Uid"].ToString();
                    }
                    txtDocNo.Text = dataRow["DocNo"].ToString();
                    txtDocNo.Tag= dataTable.Rows[0]["Uid"].ToString();
                    txtOrderNo.Text = dataRow["OrderNo"].ToString();
                    txtOrderNo.Tag = CUid;
                    DtpOrderDate.Text = dataRow["OrderDate"].ToString();
                    GrFront.Visible = false;
                    GrBack.Visible = true;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtOrderNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsDocNo.Filter = string.Format("DocNo LIKE '%{0}%'", txtOrderNo.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
