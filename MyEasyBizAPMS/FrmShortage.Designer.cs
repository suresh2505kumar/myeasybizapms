﻿namespace MyEasyBizAPMS
{
    partial class FrmShortage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer3 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer4 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.TabControlBudjet = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.txtFabric = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ttxColour = new System.Windows.Forms.TextBox();
            this.BtnFabricOk = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFabricQty = new System.Windows.Forms.TextBox();
            this.DataGridFabricPurchase = new System.Windows.Forms.DataGridView();
            this.tabPageTrims = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.txtTrims = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTrimsQty = new System.Windows.Forms.TextBox();
            this.DataGridTrims = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.SfdDataGridShortage = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.SpliAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.grBack.SuspendLayout();
            this.GrFront.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlBudjet)).BeginInit();
            this.TabControlBudjet.SuspendLayout();
            this.tabPageFabric.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricPurchase)).BeginInit();
            this.tabPageTrims.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridShortage)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(96, 34);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(936, 520);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer3.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer3;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(96, 34);
            this.SplitSave.TabIndex = 410;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.grSearch);
            this.grBack.Controls.Add(this.SplitSave);
            this.grBack.Controls.Add(this.TabControlBudjet);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.dtpDocDate);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.txtOrderNo);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Controls.Add(this.txtDocNo);
            this.grBack.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, -2);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(1050, 560);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.SpliAdd);
            this.GrFront.Controls.Add(this.SfdDataGridShortage);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(6, -2);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1050, 560);
            this.GrFront.TabIndex = 411;
            this.GrFront.TabStop = false;
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(100, 22);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(348, 224);
            this.grSearch.TabIndex = 39;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 195);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridCommon.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.EnableHeadersVisualStyles = false;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(3, 195);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // TabControlBudjet
            // 
            this.TabControlBudjet.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.BeforeTouchSize = new System.Drawing.Size(1023, 455);
            this.TabControlBudjet.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.Controls.Add(this.tabPageFabric);
            this.TabControlBudjet.Controls.Add(this.tabPageTrims);
            this.TabControlBudjet.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.Location = new System.Drawing.Point(9, 62);
            this.TabControlBudjet.Name = "TabControlBudjet";
            this.TabControlBudjet.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.TabControlBudjet.ShowSeparator = false;
            this.TabControlBudjet.Size = new System.Drawing.Size(1023, 455);
            this.TabControlBudjet.TabIndex = 409;
            // 
            // tabPageFabric
            // 
            this.tabPageFabric.Controls.Add(this.txtFabric);
            this.tabPageFabric.Controls.Add(this.label3);
            this.tabPageFabric.Controls.Add(this.label12);
            this.tabPageFabric.Controls.Add(this.ttxColour);
            this.tabPageFabric.Controls.Add(this.BtnFabricOk);
            this.tabPageFabric.Controls.Add(this.label9);
            this.tabPageFabric.Controls.Add(this.txtFabricQty);
            this.tabPageFabric.Controls.Add(this.DataGridFabricPurchase);
            this.tabPageFabric.Image = null;
            this.tabPageFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabric.Location = new System.Drawing.Point(1, 27);
            this.tabPageFabric.Name = "tabPageFabric";
            this.tabPageFabric.ShowCloseButton = true;
            this.tabPageFabric.Size = new System.Drawing.Size(1020, 426);
            this.tabPageFabric.TabIndex = 2;
            this.tabPageFabric.Text = "Fabric";
            this.tabPageFabric.ThemesEnabled = false;
            // 
            // txtFabric
            // 
            this.txtFabric.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabric.Location = new System.Drawing.Point(17, 31);
            this.txtFabric.Name = "txtFabric";
            this.txtFabric.Size = new System.Drawing.Size(460, 23);
            this.txtFabric.TabIndex = 30;
            this.txtFabric.Click += new System.EventHandler(this.TxtFabric_Click);
            this.txtFabric.TextChanged += new System.EventHandler(this.TxtFabric_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(280, 15);
            this.label3.TabIndex = 29;
            this.label3.Text = "Component / Combo / Fabric / Size / Colour / GSM";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(483, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 15);
            this.label12.TabIndex = 26;
            this.label12.Text = "Colour";
            // 
            // ttxColour
            // 
            this.ttxColour.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ttxColour.Location = new System.Drawing.Point(479, 31);
            this.ttxColour.Name = "ttxColour";
            this.ttxColour.Size = new System.Drawing.Size(118, 23);
            this.ttxColour.TabIndex = 25;
            // 
            // BtnFabricOk
            // 
            this.BtnFabricOk.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFabricOk.Location = new System.Drawing.Point(718, 30);
            this.BtnFabricOk.Name = "BtnFabricOk";
            this.BtnFabricOk.Size = new System.Drawing.Size(43, 25);
            this.BtnFabricOk.TabIndex = 24;
            this.BtnFabricOk.Text = "Ok";
            this.BtnFabricOk.UseVisualStyleBackColor = true;
            this.BtnFabricOk.Click += new System.EventHandler(this.BtnFabricOk_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(603, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 15);
            this.label9.TabIndex = 21;
            this.label9.Text = "Qty";
            // 
            // txtFabricQty
            // 
            this.txtFabricQty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFabricQty.Location = new System.Drawing.Point(599, 31);
            this.txtFabricQty.Name = "txtFabricQty";
            this.txtFabricQty.Size = new System.Drawing.Size(118, 23);
            this.txtFabricQty.TabIndex = 20;
            // 
            // DataGridFabricPurchase
            // 
            this.DataGridFabricPurchase.AllowUserToAddRows = false;
            this.DataGridFabricPurchase.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.DeepSkyBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridFabricPurchase.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DataGridFabricPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricPurchase.EnableHeadersVisualStyles = false;
            this.DataGridFabricPurchase.Location = new System.Drawing.Point(17, 57);
            this.DataGridFabricPurchase.Name = "DataGridFabricPurchase";
            this.DataGridFabricPurchase.RowHeadersVisible = false;
            this.DataGridFabricPurchase.Size = new System.Drawing.Size(949, 343);
            this.DataGridFabricPurchase.TabIndex = 18;
            // 
            // tabPageTrims
            // 
            this.tabPageTrims.Controls.Add(this.txtTrims);
            this.tabPageTrims.Controls.Add(this.label5);
            this.tabPageTrims.Controls.Add(this.button1);
            this.tabPageTrims.Controls.Add(this.label7);
            this.tabPageTrims.Controls.Add(this.txtTrimsQty);
            this.tabPageTrims.Controls.Add(this.DataGridTrims);
            this.tabPageTrims.Image = null;
            this.tabPageTrims.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageTrims.Location = new System.Drawing.Point(1, 27);
            this.tabPageTrims.Name = "tabPageTrims";
            this.tabPageTrims.ShowCloseButton = true;
            this.tabPageTrims.Size = new System.Drawing.Size(1020, 426);
            this.tabPageTrims.TabIndex = 3;
            this.tabPageTrims.Text = "Trims";
            this.tabPageTrims.ThemesEnabled = false;
            // 
            // txtTrims
            // 
            this.txtTrims.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrims.Location = new System.Drawing.Point(15, 30);
            this.txtTrims.Name = "txtTrims";
            this.txtTrims.Size = new System.Drawing.Size(460, 23);
            this.txtTrims.TabIndex = 37;
            this.txtTrims.Click += new System.EventHandler(this.TxtTrims_Click);
            this.txtTrims.TextChanged += new System.EventHandler(this.TxtTrims_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 18);
            this.label5.TabIndex = 36;
            this.label5.Text = "Trims / Combo / Size";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(595, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 25);
            this.button1.TabIndex = 33;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(476, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 32;
            this.label7.Text = "Qty";
            // 
            // txtTrimsQty
            // 
            this.txtTrimsQty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrimsQty.Location = new System.Drawing.Point(476, 30);
            this.txtTrimsQty.Name = "txtTrimsQty";
            this.txtTrimsQty.Size = new System.Drawing.Size(118, 23);
            this.txtTrimsQty.TabIndex = 31;
            // 
            // DataGridTrims
            // 
            this.DataGridTrims.AllowUserToAddRows = false;
            this.DataGridTrims.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTrims.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrims.Location = new System.Drawing.Point(15, 54);
            this.DataGridTrims.Name = "DataGridTrims";
            this.DataGridTrims.RowHeadersVisible = false;
            this.DataGridTrims.Size = new System.Drawing.Size(861, 358);
            this.DataGridTrims.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(211, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 408;
            this.label4.Text = "Doc No";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Enabled = false;
            this.dtpDocDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(468, 22);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(116, 23);
            this.dtpDocDate.TabIndex = 404;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(405, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 407;
            this.label2.Text = "Doc Date";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(69, 22);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(136, 23);
            this.txtOrderNo.TabIndex = 403;
            this.txtOrderNo.Click += new System.EventHandler(this.TxtDocNo_Click);
            this.txtOrderNo.TextChanged += new System.EventHandler(this.TxtOrderNo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 405;
            this.label1.Text = "Order No";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Enabled = false;
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(264, 22);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(136, 23);
            this.txtDocNo.TabIndex = 406;
            // 
            // SfdDataGridShortage
            // 
            this.SfdDataGridShortage.AccessibleName = "Table";
            this.SfdDataGridShortage.AllowFiltering = true;
            this.SfdDataGridShortage.Location = new System.Drawing.Point(6, 14);
            this.SfdDataGridShortage.Name = "SfdDataGridShortage";
            this.SfdDataGridShortage.Size = new System.Drawing.Size(1034, 503);
            this.SfdDataGridShortage.TabIndex = 1;
            this.SfdDataGridShortage.Text = "sfDataGrid1";
            // 
            // SpliAdd
            // 
            this.SpliAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SpliAdd.BeforeTouchSize = new System.Drawing.Size(92, 34);
            this.SpliAdd.DropDownItems.Add(this.toolstripEdit);
            this.SpliAdd.DropDownItems.Add(this.toolstripClose);
            this.SpliAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SpliAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpliAdd.ForeColor = System.Drawing.Color.Black;
            this.SpliAdd.Location = new System.Drawing.Point(780, 520);
            this.SpliAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SpliAdd.Name = "SpliAdd";
            splitButtonRenderer4.SplitButton = this.SpliAdd;
            this.SpliAdd.Renderer = splitButtonRenderer4;
            this.SpliAdd.ShowDropDownOnButtonClick = false;
            this.SpliAdd.Size = new System.Drawing.Size(92, 34);
            this.SpliAdd.TabIndex = 36;
            this.SpliAdd.Text = "Add";
            this.SpliAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SpliAdd_DropDowItemClicked);
            this.SpliAdd.Click += new System.EventHandler(this.SpliAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // FrmShortage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1058, 561);
            this.Controls.Add(this.GrFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmShortage";
            this.Text = "Shortage";
            this.Load += new System.EventHandler(this.FrmShortage_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.GrFront.ResumeLayout(false);
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlBudjet)).EndInit();
            this.TabControlBudjet.ResumeLayout(false);
            this.tabPageFabric.ResumeLayout(false);
            this.tabPageFabric.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricPurchase)).EndInit();
            this.tabPageTrims.ResumeLayout(false);
            this.tabPageTrims.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SfdDataGridShortage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDocNo;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv TabControlBudjet;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabric;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrims;
        private System.Windows.Forms.DataGridView DataGridTrims;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ttxColour;
        private System.Windows.Forms.Button BtnFabricOk;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFabricQty;
        private System.Windows.Forms.DataGridView DataGridFabricPurchase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFabric;
        private System.Windows.Forms.TextBox txtTrims;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTrimsQty;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private System.Windows.Forms.GroupBox GrFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfdDataGridShortage;
        private Syncfusion.Windows.Forms.Tools.SplitButton SpliAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
    }
}