﻿namespace MyEasyBizAPMS
{
    partial class CBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridLookup = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLookup)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridLookup
            // 
            this.DataGridLookup.AllowUserToAddRows = false;
            this.DataGridLookup.BackgroundColor = System.Drawing.Color.White;
            this.DataGridLookup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridLookup.ColumnHeadersVisible = false;
            this.DataGridLookup.Location = new System.Drawing.Point(2, 30);
            this.DataGridLookup.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridLookup.Name = "DataGridLookup";
            this.DataGridLookup.ReadOnly = true;
            this.DataGridLookup.RowHeadersVisible = false;
            this.DataGridLookup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridLookup.Size = new System.Drawing.Size(244, 213);
            this.DataGridLookup.TabIndex = 1;
            this.DataGridLookup.Visible = false;
            this.DataGridLookup.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridLookup_CellMouseDoubleClick);
            this.DataGridLookup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridLookup_KeyDown);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(2, 3);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(244, 26);
            this.textBox1.TabIndex = 2;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // CBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.DataGridLookup);
            this.Name = "CBox";
            this.Size = new System.Drawing.Size(250, 247);
            this.Load += new System.EventHandler(this.CBox_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridLookup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridLookup;
        public System.Windows.Forms.TextBox textBox1;
    }
}
