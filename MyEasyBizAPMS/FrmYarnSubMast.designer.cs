﻿namespace MyEasyBizAPMS
{
    partial class FrmYarnSubMast
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmYarnSubMast));
            this.grBack = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPer = new System.Windows.Forms.Label();
            this.txtTaxPercentage = new System.Windows.Forms.TextBox();
            this.lbltaxPer = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chckActive = new System.Windows.Forms.CheckBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panadd = new System.Windows.Forms.Panel();
            this.chckAc = new System.Windows.Forms.CheckBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.PanelgridNos = new System.Windows.Forms.Panel();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.btnNxt = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddCancel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.sfDataGridYarn = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.grBack.SuspendLayout();
            this.panadd.SuspendLayout();
            this.PanelgridNos.SuspendLayout();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridYarn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.SuspendLayout();
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.lblPer);
            this.grBack.Controls.Add(this.txtTaxPercentage);
            this.grBack.Controls.Add(this.lbltaxPer);
            this.grBack.Controls.Add(this.txtShortName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.chckActive);
            this.grBack.Controls.Add(this.txtName);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, 4);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(522, 346);
            this.grBack.TabIndex = 0;
            this.grBack.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(336, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "* indicates mandatory fields";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(431, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(430, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "*";
            // 
            // lblPer
            // 
            this.lblPer.AutoSize = true;
            this.lblPer.Location = new System.Drawing.Point(252, 180);
            this.lblPer.Name = "lblPer";
            this.lblPer.Size = new System.Drawing.Size(19, 18);
            this.lblPer.TabIndex = 6;
            this.lblPer.Text = "%";
            this.lblPer.Visible = false;
            // 
            // txtTaxPercentage
            // 
            this.txtTaxPercentage.Location = new System.Drawing.Point(184, 176);
            this.txtTaxPercentage.Name = "txtTaxPercentage";
            this.txtTaxPercentage.Size = new System.Drawing.Size(65, 26);
            this.txtTaxPercentage.TabIndex = 4;
            this.txtTaxPercentage.Text = "0";
            this.txtTaxPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxPercentage.Visible = false;
            // 
            // lbltaxPer
            // 
            this.lbltaxPer.AutoSize = true;
            this.lbltaxPer.Location = new System.Drawing.Point(76, 179);
            this.lbltaxPer.Name = "lbltaxPer";
            this.lbltaxPer.Size = new System.Drawing.Size(101, 18);
            this.lbltaxPer.TabIndex = 5;
            this.lbltaxPer.Text = "Tax Percentage";
            this.lbltaxPer.Visible = false;
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(184, 139);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(242, 26);
            this.txtShortName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Short Name";
            // 
            // chckActive
            // 
            this.chckActive.AutoSize = true;
            this.chckActive.Location = new System.Drawing.Point(184, 212);
            this.chckActive.Name = "chckActive";
            this.chckActive.Size = new System.Drawing.Size(65, 22);
            this.chckActive.TabIndex = 2;
            this.chckActive.Text = "Active";
            this.chckActive.UseVisualStyleBackColor = true;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(184, 99);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(242, 26);
            this.txtName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // panadd
            // 
            this.panadd.BackColor = System.Drawing.Color.White;
            this.panadd.Controls.Add(this.chckAc);
            this.panadd.Controls.Add(this.btnAdd);
            this.panadd.Controls.Add(this.PanelgridNos);
            this.panadd.Controls.Add(this.btnFirst);
            this.panadd.Controls.Add(this.btnBack);
            this.panadd.Controls.Add(this.btnLast);
            this.panadd.Controls.Add(this.btnNxt);
            this.panadd.Controls.Add(this.btnEdit);
            this.panadd.Controls.Add(this.btnSave);
            this.panadd.Controls.Add(this.btnAddCancel);
            this.panadd.Controls.Add(this.btnExit);
            this.panadd.Location = new System.Drawing.Point(4, 353);
            this.panadd.Name = "panadd";
            this.panadd.Size = new System.Drawing.Size(524, 36);
            this.panadd.TabIndex = 239;
            // 
            // chckAc
            // 
            this.chckAc.AutoSize = true;
            this.chckAc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckAc.Location = new System.Drawing.Point(186, 7);
            this.chckAc.Name = "chckAc";
            this.chckAc.Size = new System.Drawing.Size(65, 22);
            this.chckAc.TabIndex = 215;
            this.chckAc.Text = "Active";
            this.chckAc.UseVisualStyleBackColor = true;
            this.chckAc.CheckedChanged += new System.EventHandler(this.ChckAc_CheckedChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(320, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 31);
            this.btnAdd.TabIndex = 184;
            this.btnAdd.Text = "Add new";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // PanelgridNos
            // 
            this.PanelgridNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PanelgridNos.Controls.Add(this.lblFrom);
            this.PanelgridNos.Controls.Add(this.lblCount);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel3);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel2);
            this.PanelgridNos.Controls.Add(this.flowLayoutPanel1);
            this.PanelgridNos.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PanelgridNos.Location = new System.Drawing.Point(49, 2);
            this.PanelgridNos.Name = "PanelgridNos";
            this.PanelgridNos.Size = new System.Drawing.Size(74, 30);
            this.PanelgridNos.TabIndex = 214;
            this.PanelgridNos.Visible = false;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.ForeColor = System.Drawing.Color.Black;
            this.lblFrom.Location = new System.Drawing.Point(4, 5);
            this.lblFrom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(15, 18);
            this.lblFrom.TabIndex = 163;
            this.lblFrom.Text = "1";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Black;
            this.lblCount.Location = new System.Drawing.Point(27, 5);
            this.lblCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(31, 18);
            this.lblCount.TabIndex = 162;
            this.lblCount.Text = "of 1";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(61, 30);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(38, 35);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(62, -129);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnFirst
            // 
            this.btnFirst.BackColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnFirst.FlatAppearance.BorderSize = 0;
            this.btnFirst.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnFirst.Image")));
            this.btnFirst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFirst.Location = new System.Drawing.Point(6, 2);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(19, 31);
            this.btnFirst.TabIndex = 213;
            this.btnFirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFirst.UseVisualStyleBackColor = false;
            this.btnFirst.Visible = false;
            this.btnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(26, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(18, 31);
            this.btnBack.TabIndex = 212;
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // btnLast
            // 
            this.btnLast.BackColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLast.FlatAppearance.BorderSize = 0;
            this.btnLast.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLast.Image = ((System.Drawing.Image)(resources.GetObject("btnLast.Image")));
            this.btnLast.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLast.Location = new System.Drawing.Point(147, 2);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(19, 31);
            this.btnLast.TabIndex = 211;
            this.btnLast.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLast.UseVisualStyleBackColor = false;
            this.btnLast.Visible = false;
            this.btnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // btnNxt
            // 
            this.btnNxt.BackColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNxt.FlatAppearance.BorderSize = 0;
            this.btnNxt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNxt.Image = ((System.Drawing.Image)(resources.GetObject("btnNxt.Image")));
            this.btnNxt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNxt.Location = new System.Drawing.Point(127, 2);
            this.btnNxt.Name = "btnNxt";
            this.btnNxt.Size = new System.Drawing.Size(18, 31);
            this.btnNxt.TabIndex = 210;
            this.btnNxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNxt.UseVisualStyleBackColor = false;
            this.btnNxt.Visible = false;
            this.btnNxt.Click += new System.EventHandler(this.BtnNxt_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.White;
            this.btnEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(406, 3);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(60, 31);
            this.btnEdit.TabIndex = 185;
            this.btnEdit.Text = "Edit";
            this.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(371, 3);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnAddCancel
            // 
            this.btnAddCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCancel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCancel.Image")));
            this.btnAddCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCancel.Location = new System.Drawing.Point(450, 3);
            this.btnAddCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCancel.Name = "btnAddCancel";
            this.btnAddCancel.Size = new System.Drawing.Size(60, 31);
            this.btnAddCancel.TabIndex = 9;
            this.btnAddCancel.Text = "Back";
            this.btnAddCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddCancel.UseVisualStyleBackColor = false;
            this.btnAddCancel.Click += new System.EventHandler(this.BtnAddCancel_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(464, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(57, 31);
            this.btnExit.TabIndex = 208;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.sfDataGridYarn);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridCommon);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(522, 345);
            this.grFront.TabIndex = 3;
            this.grFront.TabStop = false;
            // 
            // sfDataGridYarn
            // 
            this.sfDataGridYarn.AccessibleName = "Table";
            this.sfDataGridYarn.AllowFiltering = true;
            this.sfDataGridYarn.Location = new System.Drawing.Point(6, 13);
            this.sfDataGridYarn.Name = "sfDataGridYarn";
            this.sfDataGridYarn.PasteOption = Syncfusion.WinForms.DataGrid.Enums.PasteOptions.None;
            this.sfDataGridYarn.Size = new System.Drawing.Size(514, 325);
            this.sfDataGridYarn.TabIndex = 1;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(7, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(513, 26);
            this.txtSearch.TabIndex = 3;
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(7, 40);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(513, 298);
            this.DataGridCommon.TabIndex = 2;
            this.DataGridCommon.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseClick);
            // 
            // FrmYarnSubMast
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(535, 397);
            this.Controls.Add(this.panadd);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmYarnSubMast";
            this.Text = "PLY Master";
            this.Load += new System.EventHandler(this.FrmYarnSubMast_Load);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            this.panadd.ResumeLayout(false);
            this.panadd.PerformLayout();
            this.PanelgridNos.ResumeLayout(false);
            this.PanelgridNos.PerformLayout();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sfDataGridYarn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.Panel panadd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddCancel;
        private System.Windows.Forms.Panel PanelgridNos;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Button btnNxt;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chckActive;
        private System.Windows.Forms.CheckBox chckAc;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.TextBox txtSearch;
        private Syncfusion.WinForms.DataGrid.SfDataGrid sfDataGridYarn;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTaxPercentage;
        private System.Windows.Forms.Label lbltaxPer;
        private System.Windows.Forms.Label lblPer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}