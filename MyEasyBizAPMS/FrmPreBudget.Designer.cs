﻿namespace MyEasyBizAPMS
{
    partial class FrmPreBudget
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer1 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            Syncfusion.Windows.Forms.Tools.SplitButtonRenderer splitButtonRenderer2 = new Syncfusion.Windows.Forms.Tools.SplitButtonRenderer();
            this.SplitSave = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripBack = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.SplitAdd = new Syncfusion.Windows.Forms.Tools.SplitButton();
            this.toolstripEdit = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.toolstripClose = new Syncfusion.Windows.Forms.Tools.toolstripitem();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.grSearch = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.btnHide = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDocDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbStyle = new System.Windows.Forms.ComboBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.txtGrossValue = new System.Windows.Forms.TextBox();
            this.txtExrate = new System.Windows.Forms.TextBox();
            this.txtPcrate = new System.Windows.Forms.TextBox();
            this.TabControlBudjet = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageYarn = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.tabContronYan = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageYarnPurchase = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.ChckYarnRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridYarnPurchase = new System.Windows.Forms.DataGridView();
            this.BtnYarnProcessOk = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPurchaseRate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtYarnPurchaseQty = new System.Windows.Forms.TextBox();
            this.CmbYarn = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPageYanProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.ChckYarnProcessRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridYarnProcess = new System.Windows.Forms.DataGridView();
            this.tabPageFabric = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.tabControlFabric = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageFabricProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.ChckFabricRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridFabricProcess = new System.Windows.Forms.DataGridView();
            this.tabPageFabricPurchase = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BtnFabricOk = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFabricAmount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFabricQty = new System.Windows.Forms.TextBox();
            this.CmbFabricPurchase = new System.Windows.Forms.ComboBox();
            this.DataGridFabricPurchase = new System.Windows.Forms.DataGridView();
            this.tabPageTrims = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.ChckTrimsRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridTrims = new System.Windows.Forms.DataGridView();
            this.tabPageTrimsProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ChckTrimsProcessRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridTrimsProcess = new System.Windows.Forms.DataGridView();
            this.tabPageGarmentProcess = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ChckGarmenrRefresh = new System.Windows.Forms.CheckBox();
            this.DataGridGarmentProcess = new System.Windows.Forms.DataGridView();
            this.tabPageCMT = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BtnCMTOK = new System.Windows.Forms.Button();
            this.txtCMTRate = new System.Windows.Forms.TextBox();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.CmbOperations = new System.Windows.Forms.ComboBox();
            this.CmbCOORDINATES = new System.Windows.Forms.ComboBox();
            this.DataGridCMT = new System.Windows.Forms.DataGridView();
            this.tabPageOthers = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.grOtherExp = new System.Windows.Forms.GroupBox();
            this.BtnExpensesOk = new System.Windows.Forms.Button();
            this.lblRate = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.CmbType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.CmbOtherExpHead = new System.Windows.Forms.ComboBox();
            this.DataGridOtherExpenses = new System.Windows.Forms.DataGridView();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.txtPercentage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.BtnPrintgarment = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.SfDataGridPreBudjet = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.GrBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlBudjet)).BeginInit();
            this.TabControlBudjet.SuspendLayout();
            this.tabPageYarn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabContronYan)).BeginInit();
            this.tabContronYan.SuspendLayout();
            this.tabPageYarnPurchase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnPurchase)).BeginInit();
            this.tabPageYanProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcess)).BeginInit();
            this.tabPageFabric.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlFabric)).BeginInit();
            this.tabControlFabric.SuspendLayout();
            this.tabPageFabricProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricProcess)).BeginInit();
            this.tabPageFabricPurchase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricPurchase)).BeginInit();
            this.tabPageTrims.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).BeginInit();
            this.tabPageTrimsProcess.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsProcess)).BeginInit();
            this.tabPageGarmentProcess.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarmentProcess)).BeginInit();
            this.tabPageCMT.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCMT)).BeginInit();
            this.tabPageOthers.SuspendLayout();
            this.grOtherExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOtherExpenses)).BeginInit();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPreBudjet)).BeginInit();
            this.SuspendLayout();
            // 
            // SplitSave
            // 
            this.SplitSave.BackColor = System.Drawing.SystemColors.Control;
            this.SplitSave.BeforeTouchSize = new System.Drawing.Size(96, 34);
            this.SplitSave.DropDownItems.Add(this.toolstripBack);
            this.SplitSave.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitSave.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitSave.ForeColor = System.Drawing.Color.Black;
            this.SplitSave.Location = new System.Drawing.Point(994, 539);
            this.SplitSave.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitSave.Name = "SplitSave";
            splitButtonRenderer1.SplitButton = this.SplitSave;
            this.SplitSave.Renderer = splitButtonRenderer1;
            this.SplitSave.ShowDropDownOnButtonClick = false;
            this.SplitSave.Size = new System.Drawing.Size(96, 34);
            this.SplitSave.TabIndex = 406;
            this.SplitSave.Text = "Save";
            this.SplitSave.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitSave_DropDowItemClicked);
            this.SplitSave.Click += new System.EventHandler(this.SplitSave_Click);
            // 
            // toolstripBack
            // 
            this.toolstripBack.Name = "toolstripBack";
            this.toolstripBack.Size = new System.Drawing.Size(23, 23);
            this.toolstripBack.Text = "Back";
            // 
            // SplitAdd
            // 
            this.SplitAdd.BackColor = System.Drawing.SystemColors.Control;
            this.SplitAdd.BeforeTouchSize = new System.Drawing.Size(95, 35);
            this.SplitAdd.DropDownItems.Add(this.toolstripEdit);
            this.SplitAdd.DropDownItems.Add(this.toolstripClose);
            this.SplitAdd.DropDownPosition = Syncfusion.Windows.Forms.Tools.Position.Bottom;
            this.SplitAdd.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SplitAdd.ForeColor = System.Drawing.Color.Black;
            this.SplitAdd.Location = new System.Drawing.Point(867, 535);
            this.SplitAdd.MinimumSize = new System.Drawing.Size(75, 23);
            this.SplitAdd.Name = "SplitAdd";
            splitButtonRenderer2.SplitButton = this.SplitAdd;
            this.SplitAdd.Renderer = splitButtonRenderer2;
            this.SplitAdd.ShowDropDownOnButtonClick = false;
            this.SplitAdd.Size = new System.Drawing.Size(95, 35);
            this.SplitAdd.TabIndex = 1;
            this.SplitAdd.Text = "Add";
            this.SplitAdd.DropDowItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.SplitAdd_DropDowItemClicked);
            this.SplitAdd.Click += new System.EventHandler(this.SplitAdd_Click);
            // 
            // toolstripEdit
            // 
            this.toolstripEdit.Name = "toolstripEdit";
            this.toolstripEdit.Size = new System.Drawing.Size(23, 23);
            this.toolstripEdit.Text = "Edit";
            // 
            // toolstripClose
            // 
            this.toolstripClose.Name = "toolstripClose";
            this.toolstripClose.Size = new System.Drawing.Size(23, 23);
            this.toolstripClose.Text = "Close";
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.label16);
            this.GrBack.Controls.Add(this.label15);
            this.GrBack.Controls.Add(this.label13);
            this.GrBack.Controls.Add(this.SplitSave);
            this.GrBack.Controls.Add(this.grSearch);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.dtpDocDate);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.txtDocNo);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.CmbStyle);
            this.GrBack.Controls.Add(this.txtOrderNo);
            this.GrBack.Controls.Add(this.txtGrossValue);
            this.GrBack.Controls.Add(this.txtExrate);
            this.GrBack.Controls.Add(this.txtPcrate);
            this.GrBack.Controls.Add(this.TabControlBudjet);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(9, 5);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(1108, 578);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(983, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 15);
            this.label16.TabIndex = 411;
            this.label16.Text = "Gross Value";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(914, 12);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 15);
            this.label15.TabIndex = 410;
            this.label15.Text = "Ex Rate";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(842, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 15);
            this.label13.TabIndex = 408;
            this.label13.Text = "Pc Rate";
            // 
            // grSearch
            // 
            this.grSearch.Controls.Add(this.btnSelect);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Location = new System.Drawing.Point(592, 17);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(348, 225);
            this.grSearch.TabIndex = 37;
            this.grSearch.TabStop = false;
            this.grSearch.Visible = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnSelect.Location = new System.Drawing.Point(281, 195);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 28);
            this.btnSelect.TabIndex = 396;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.Location = new System.Drawing.Point(4, 12);
            this.DataGridCommon.MultiSelect = false;
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(336, 183);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(3, 195);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(65, 28);
            this.btnHide.TabIndex = 395;
            this.btnHide.Text = "Close";
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.BtnHide_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(218, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 402;
            this.label4.Text = "Doc No";
            // 
            // dtpDocDate
            // 
            this.dtpDocDate.Enabled = false;
            this.dtpDocDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDocDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDocDate.Location = new System.Drawing.Point(470, 17);
            this.dtpDocDate.Name = "dtpDocDate";
            this.dtpDocDate.Size = new System.Drawing.Size(116, 23);
            this.dtpDocDate.TabIndex = 398;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(407, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 401;
            this.label2.Text = "Doc Date";
            // 
            // txtDocNo
            // 
            this.txtDocNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocNo.Location = new System.Drawing.Point(76, 17);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(136, 23);
            this.txtDocNo.TabIndex = 397;
            this.txtDocNo.Click += new System.EventHandler(this.TxtDocNo_Click);
            this.txtDocNo.TextChanged += new System.EventHandler(this.txtDocNo_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 399;
            this.label1.Text = "Order No";
            // 
            // CmbStyle
            // 
            this.CmbStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStyle.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbStyle.FormattingEnabled = true;
            this.CmbStyle.Location = new System.Drawing.Point(628, 17);
            this.CmbStyle.Name = "CmbStyle";
            this.CmbStyle.Size = new System.Drawing.Size(205, 23);
            this.CmbStyle.TabIndex = 403;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Enabled = false;
            this.txtOrderNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderNo.Location = new System.Drawing.Point(266, 17);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(136, 23);
            this.txtOrderNo.TabIndex = 400;
            // 
            // txtGrossValue
            // 
            this.txtGrossValue.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossValue.Location = new System.Drawing.Point(983, 30);
            this.txtGrossValue.Name = "txtGrossValue";
            this.txtGrossValue.Size = new System.Drawing.Size(119, 23);
            this.txtGrossValue.TabIndex = 412;
            // 
            // txtExrate
            // 
            this.txtExrate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExrate.Location = new System.Drawing.Point(914, 30);
            this.txtExrate.Name = "txtExrate";
            this.txtExrate.Size = new System.Drawing.Size(68, 23);
            this.txtExrate.TabIndex = 409;
            this.txtExrate.TextChanged += new System.EventHandler(this.txtExrate_TextChanged);
            // 
            // txtPcrate
            // 
            this.txtPcrate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPcrate.Location = new System.Drawing.Point(842, 30);
            this.txtPcrate.Name = "txtPcrate";
            this.txtPcrate.Size = new System.Drawing.Size(71, 23);
            this.txtPcrate.TabIndex = 407;
            this.txtPcrate.TextChanged += new System.EventHandler(this.TxtPcrate_TextChanged);
            // 
            // TabControlBudjet
            // 
            this.TabControlBudjet.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.BeforeTouchSize = new System.Drawing.Size(1095, 481);
            this.TabControlBudjet.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.Controls.Add(this.tabPageYarn);
            this.TabControlBudjet.Controls.Add(this.tabPageFabric);
            this.TabControlBudjet.Controls.Add(this.tabPageTrims);
            this.TabControlBudjet.Controls.Add(this.tabPageTrimsProcess);
            this.TabControlBudjet.Controls.Add(this.tabPageGarmentProcess);
            this.TabControlBudjet.Controls.Add(this.tabPageCMT);
            this.TabControlBudjet.Controls.Add(this.tabPageOthers);
            this.TabControlBudjet.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.TabControlBudjet.Location = new System.Drawing.Point(6, 54);
            this.TabControlBudjet.Name = "TabControlBudjet";
            this.TabControlBudjet.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.TabControlBudjet.ShowSeparator = false;
            this.TabControlBudjet.Size = new System.Drawing.Size(1095, 481);
            this.TabControlBudjet.TabIndex = 405;
            this.TabControlBudjet.SelectedIndexChanged += new System.EventHandler(this.TabControlBudjet_SelectedIndexChanged);
            // 
            // tabPageYarn
            // 
            this.tabPageYarn.Controls.Add(this.tabContronYan);
            this.tabPageYarn.Image = null;
            this.tabPageYarn.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageYarn.Location = new System.Drawing.Point(1, 30);
            this.tabPageYarn.Name = "tabPageYarn";
            this.tabPageYarn.ShowCloseButton = true;
            this.tabPageYarn.Size = new System.Drawing.Size(1092, 449);
            this.tabPageYarn.TabIndex = 1;
            this.tabPageYarn.Text = "Yarn";
            this.tabPageYarn.ThemesEnabled = false;
            // 
            // tabContronYan
            // 
            this.tabContronYan.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabContronYan.BeforeTouchSize = new System.Drawing.Size(1081, 438);
            this.tabContronYan.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabContronYan.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabContronYan.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabContronYan.Controls.Add(this.tabPageYarnPurchase);
            this.tabContronYan.Controls.Add(this.tabPageYanProcess);
            this.tabContronYan.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabContronYan.Location = new System.Drawing.Point(6, 9);
            this.tabContronYan.Name = "tabContronYan";
            this.tabContronYan.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabContronYan.ShowSeparator = false;
            this.tabContronYan.Size = new System.Drawing.Size(1081, 438);
            this.tabContronYan.TabIndex = 0;
            this.tabContronYan.SelectedIndexChanged += new System.EventHandler(this.TabContronYan_SelectedIndexChanged);
            // 
            // tabPageYarnPurchase
            // 
            this.tabPageYarnPurchase.Controls.Add(this.ChckYarnRefresh);
            this.tabPageYarnPurchase.Controls.Add(this.DataGridYarnPurchase);
            this.tabPageYarnPurchase.Controls.Add(this.BtnYarnProcessOk);
            this.tabPageYarnPurchase.Controls.Add(this.label7);
            this.tabPageYarnPurchase.Controls.Add(this.txtPurchaseRate);
            this.tabPageYarnPurchase.Controls.Add(this.label6);
            this.tabPageYarnPurchase.Controls.Add(this.txtYarnPurchaseQty);
            this.tabPageYarnPurchase.Controls.Add(this.CmbYarn);
            this.tabPageYarnPurchase.Controls.Add(this.label5);
            this.tabPageYarnPurchase.Image = null;
            this.tabPageYarnPurchase.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageYarnPurchase.Location = new System.Drawing.Point(1, 30);
            this.tabPageYarnPurchase.Name = "tabPageYarnPurchase";
            this.tabPageYarnPurchase.ShowCloseButton = true;
            this.tabPageYarnPurchase.Size = new System.Drawing.Size(1078, 406);
            this.tabPageYarnPurchase.TabIndex = 1;
            this.tabPageYarnPurchase.Text = "Yarn Purchase";
            this.tabPageYarnPurchase.ThemesEnabled = false;
            this.tabPageYarnPurchase.Click += new System.EventHandler(this.TabPageYarnPurchase_Click);
            // 
            // ChckYarnRefresh
            // 
            this.ChckYarnRefresh.AutoSize = true;
            this.ChckYarnRefresh.Location = new System.Drawing.Point(942, 10);
            this.ChckYarnRefresh.Name = "ChckYarnRefresh";
            this.ChckYarnRefresh.Size = new System.Drawing.Size(104, 22);
            this.ChckYarnRefresh.TabIndex = 9;
            this.ChckYarnRefresh.Text = "Yarn Refresh";
            this.ChckYarnRefresh.UseVisualStyleBackColor = true;
            this.ChckYarnRefresh.CheckedChanged += new System.EventHandler(this.ChckYarnRefresh_CheckedChanged);
            // 
            // DataGridYarnPurchase
            // 
            this.DataGridYarnPurchase.AllowUserToAddRows = false;
            this.DataGridYarnPurchase.BackgroundColor = System.Drawing.Color.White;
            this.DataGridYarnPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnPurchase.Location = new System.Drawing.Point(6, 5);
            this.DataGridYarnPurchase.Name = "DataGridYarnPurchase";
            this.DataGridYarnPurchase.RowHeadersVisible = false;
            this.DataGridYarnPurchase.Size = new System.Drawing.Size(930, 397);
            this.DataGridYarnPurchase.TabIndex = 0;
            this.DataGridYarnPurchase.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridYarnPurchase_CellValueChanged);
            this.DataGridYarnPurchase.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridYarnPurchase_DataError);
            // 
            // BtnYarnProcessOk
            // 
            this.BtnYarnProcessOk.Location = new System.Drawing.Point(610, 25);
            this.BtnYarnProcessOk.Name = "BtnYarnProcessOk";
            this.BtnYarnProcessOk.Size = new System.Drawing.Size(43, 28);
            this.BtnYarnProcessOk.TabIndex = 8;
            this.BtnYarnProcessOk.Text = "Ok";
            this.BtnYarnProcessOk.UseVisualStyleBackColor = true;
            this.BtnYarnProcessOk.Click += new System.EventHandler(this.BtnYarnProcessOk_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(495, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "Amount";
            // 
            // txtPurchaseRate
            // 
            this.txtPurchaseRate.Location = new System.Drawing.Point(495, 26);
            this.txtPurchaseRate.Name = "txtPurchaseRate";
            this.txtPurchaseRate.Size = new System.Drawing.Size(114, 26);
            this.txtPurchaseRate.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(378, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Qty";
            // 
            // txtYarnPurchaseQty
            // 
            this.txtYarnPurchaseQty.Location = new System.Drawing.Point(378, 26);
            this.txtYarnPurchaseQty.Name = "txtYarnPurchaseQty";
            this.txtYarnPurchaseQty.Size = new System.Drawing.Size(116, 26);
            this.txtYarnPurchaseQty.TabIndex = 4;
            // 
            // CmbYarn
            // 
            this.CmbYarn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbYarn.FormattingEnabled = true;
            this.CmbYarn.Location = new System.Drawing.Point(7, 26);
            this.CmbYarn.Name = "CmbYarn";
            this.CmbYarn.Size = new System.Drawing.Size(370, 26);
            this.CmbYarn.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "YarnName";
            // 
            // tabPageYanProcess
            // 
            this.tabPageYanProcess.Controls.Add(this.ChckYarnProcessRefresh);
            this.tabPageYanProcess.Controls.Add(this.DataGridYarnProcess);
            this.tabPageYanProcess.Image = null;
            this.tabPageYanProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageYanProcess.Location = new System.Drawing.Point(1, 30);
            this.tabPageYanProcess.Name = "tabPageYanProcess";
            this.tabPageYanProcess.ShowCloseButton = true;
            this.tabPageYanProcess.Size = new System.Drawing.Size(1078, 406);
            this.tabPageYanProcess.TabIndex = 2;
            this.tabPageYanProcess.Text = "Yarn Process";
            this.tabPageYanProcess.ThemesEnabled = false;
            // 
            // ChckYarnProcessRefresh
            // 
            this.ChckYarnProcessRefresh.AutoSize = true;
            this.ChckYarnProcessRefresh.Location = new System.Drawing.Point(921, 1);
            this.ChckYarnProcessRefresh.Name = "ChckYarnProcessRefresh";
            this.ChckYarnProcessRefresh.Size = new System.Drawing.Size(154, 22);
            this.ChckYarnProcessRefresh.TabIndex = 10;
            this.ChckYarnProcessRefresh.Text = "Yarn Process Refresh";
            this.ChckYarnProcessRefresh.UseVisualStyleBackColor = true;
            this.ChckYarnProcessRefresh.CheckedChanged += new System.EventHandler(this.ChckYarnProcessRefresh_CheckedChanged);
            // 
            // DataGridYarnProcess
            // 
            this.DataGridYarnProcess.AllowUserToAddRows = false;
            this.DataGridYarnProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridYarnProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridYarnProcess.Location = new System.Drawing.Point(7, 24);
            this.DataGridYarnProcess.Name = "DataGridYarnProcess";
            this.DataGridYarnProcess.RowHeadersVisible = false;
            this.DataGridYarnProcess.Size = new System.Drawing.Size(929, 366);
            this.DataGridYarnProcess.TabIndex = 1;
            this.DataGridYarnProcess.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridYarnProcess_CellValueChanged);
            this.DataGridYarnProcess.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridYarnProcess_DataError);
            // 
            // tabPageFabric
            // 
            this.tabPageFabric.Controls.Add(this.tabControlFabric);
            this.tabPageFabric.Image = null;
            this.tabPageFabric.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabric.Location = new System.Drawing.Point(1, 30);
            this.tabPageFabric.Name = "tabPageFabric";
            this.tabPageFabric.ShowCloseButton = true;
            this.tabPageFabric.Size = new System.Drawing.Size(1092, 449);
            this.tabPageFabric.TabIndex = 2;
            this.tabPageFabric.Text = "Fabric";
            this.tabPageFabric.ThemesEnabled = false;
            // 
            // tabControlFabric
            // 
            this.tabControlFabric.ActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlFabric.BeforeTouchSize = new System.Drawing.Size(1081, 471);
            this.tabControlFabric.CloseButtonForeColor = System.Drawing.Color.Empty;
            this.tabControlFabric.CloseButtonHoverForeColor = System.Drawing.Color.Empty;
            this.tabControlFabric.CloseButtonPressedForeColor = System.Drawing.Color.Empty;
            this.tabControlFabric.Controls.Add(this.tabPageFabricProcess);
            this.tabControlFabric.Controls.Add(this.tabPageFabricPurchase);
            this.tabControlFabric.InActiveTabForeColor = System.Drawing.Color.Empty;
            this.tabControlFabric.Location = new System.Drawing.Point(6, 7);
            this.tabControlFabric.Name = "tabControlFabric";
            this.tabControlFabric.SeparatorColor = System.Drawing.SystemColors.ControlDark;
            this.tabControlFabric.ShowSeparator = false;
            this.tabControlFabric.Size = new System.Drawing.Size(1081, 471);
            this.tabControlFabric.TabIndex = 1;
            this.tabControlFabric.SelectedIndexChanged += new System.EventHandler(this.TabControlFabric_SelectedIndexChanged);
            // 
            // tabPageFabricProcess
            // 
            this.tabPageFabricProcess.Controls.Add(this.ChckFabricRefresh);
            this.tabPageFabricProcess.Controls.Add(this.DataGridFabricProcess);
            this.tabPageFabricProcess.Image = null;
            this.tabPageFabricProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabricProcess.Location = new System.Drawing.Point(1, 0);
            this.tabPageFabricProcess.Name = "tabPageFabricProcess";
            this.tabPageFabricProcess.ShowCloseButton = true;
            this.tabPageFabricProcess.Size = new System.Drawing.Size(1078, 469);
            this.tabPageFabricProcess.TabIndex = 2;
            this.tabPageFabricProcess.Text = "Fabric Process";
            this.tabPageFabricProcess.ThemesEnabled = false;
            // 
            // ChckFabricRefresh
            // 
            this.ChckFabricRefresh.AutoSize = true;
            this.ChckFabricRefresh.Location = new System.Drawing.Point(927, 5);
            this.ChckFabricRefresh.Name = "ChckFabricRefresh";
            this.ChckFabricRefresh.Size = new System.Drawing.Size(115, 22);
            this.ChckFabricRefresh.TabIndex = 3;
            this.ChckFabricRefresh.Text = "Fabric Refresh";
            this.ChckFabricRefresh.UseVisualStyleBackColor = true;
            this.ChckFabricRefresh.CheckedChanged += new System.EventHandler(this.ChckFabricRefresh_CheckedChanged);
            // 
            // DataGridFabricProcess
            // 
            this.DataGridFabricProcess.AllowUserToAddRows = false;
            this.DataGridFabricProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridFabricProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricProcess.Location = new System.Drawing.Point(7, 31);
            this.DataGridFabricProcess.Name = "DataGridFabricProcess";
            this.DataGridFabricProcess.RowHeadersVisible = false;
            this.DataGridFabricProcess.Size = new System.Drawing.Size(1033, 369);
            this.DataGridFabricProcess.TabIndex = 2;
            this.DataGridFabricProcess.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridFabricProcess_CellValueChanged);
            this.DataGridFabricProcess.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridFabricProcess_DataError);
            // 
            // tabPageFabricPurchase
            // 
            this.tabPageFabricPurchase.Controls.Add(this.label14);
            this.tabPageFabricPurchase.Controls.Add(this.textBox2);
            this.tabPageFabricPurchase.Controls.Add(this.label12);
            this.tabPageFabricPurchase.Controls.Add(this.textBox1);
            this.tabPageFabricPurchase.Controls.Add(this.BtnFabricOk);
            this.tabPageFabricPurchase.Controls.Add(this.label8);
            this.tabPageFabricPurchase.Controls.Add(this.txtFabricAmount);
            this.tabPageFabricPurchase.Controls.Add(this.label9);
            this.tabPageFabricPurchase.Controls.Add(this.txtFabricQty);
            this.tabPageFabricPurchase.Controls.Add(this.CmbFabricPurchase);
            this.tabPageFabricPurchase.Controls.Add(this.DataGridFabricPurchase);
            this.tabPageFabricPurchase.Image = null;
            this.tabPageFabricPurchase.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageFabricPurchase.Location = new System.Drawing.Point(1, 0);
            this.tabPageFabricPurchase.Name = "tabPageFabricPurchase";
            this.tabPageFabricPurchase.ShowCloseButton = true;
            this.tabPageFabricPurchase.Size = new System.Drawing.Size(1078, 469);
            this.tabPageFabricPurchase.TabIndex = 1;
            this.tabPageFabricPurchase.Text = "Fabric Purchase";
            this.tabPageFabricPurchase.ThemesEnabled = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(591, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 18);
            this.label14.TabIndex = 17;
            this.label14.Text = "Process";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(593, 24);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(147, 26);
            this.textBox2.TabIndex = 16;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(470, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 18);
            this.label12.TabIndex = 15;
            this.label12.Text = "Colour";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(473, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(118, 26);
            this.textBox1.TabIndex = 14;
            // 
            // BtnFabricOk
            // 
            this.BtnFabricOk.Location = new System.Drawing.Point(975, 23);
            this.BtnFabricOk.Name = "BtnFabricOk";
            this.BtnFabricOk.Size = new System.Drawing.Size(43, 28);
            this.BtnFabricOk.TabIndex = 13;
            this.BtnFabricOk.Text = "Ok";
            this.BtnFabricOk.UseVisualStyleBackColor = true;
            this.BtnFabricOk.Click += new System.EventHandler(this.BtnFabricOk_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(858, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 18);
            this.label8.TabIndex = 12;
            this.label8.Text = "Amount";
            // 
            // txtFabricAmount
            // 
            this.txtFabricAmount.Location = new System.Drawing.Point(861, 24);
            this.txtFabricAmount.Name = "txtFabricAmount";
            this.txtFabricAmount.Size = new System.Drawing.Size(114, 26);
            this.txtFabricAmount.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(739, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 18);
            this.label9.TabIndex = 10;
            this.label9.Text = "Qty";
            // 
            // txtFabricQty
            // 
            this.txtFabricQty.Location = new System.Drawing.Point(742, 24);
            this.txtFabricQty.Name = "txtFabricQty";
            this.txtFabricQty.Size = new System.Drawing.Size(118, 26);
            this.txtFabricQty.TabIndex = 9;
            // 
            // CmbFabricPurchase
            // 
            this.CmbFabricPurchase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbFabricPurchase.FormattingEnabled = true;
            this.CmbFabricPurchase.Location = new System.Drawing.Point(7, 24);
            this.CmbFabricPurchase.Name = "CmbFabricPurchase";
            this.CmbFabricPurchase.Size = new System.Drawing.Size(463, 26);
            this.CmbFabricPurchase.TabIndex = 3;
            // 
            // DataGridFabricPurchase
            // 
            this.DataGridFabricPurchase.AllowUserToAddRows = false;
            this.DataGridFabricPurchase.BackgroundColor = System.Drawing.Color.White;
            this.DataGridFabricPurchase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridFabricPurchase.Location = new System.Drawing.Point(7, 53);
            this.DataGridFabricPurchase.Name = "DataGridFabricPurchase";
            this.DataGridFabricPurchase.RowHeadersVisible = false;
            this.DataGridFabricPurchase.Size = new System.Drawing.Size(1008, 329);
            this.DataGridFabricPurchase.TabIndex = 2;
            this.DataGridFabricPurchase.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridFabricPurchase_DataError);
            // 
            // tabPageTrims
            // 
            this.tabPageTrims.Controls.Add(this.ChckTrimsRefresh);
            this.tabPageTrims.Controls.Add(this.DataGridTrims);
            this.tabPageTrims.Image = null;
            this.tabPageTrims.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageTrims.Location = new System.Drawing.Point(1, 30);
            this.tabPageTrims.Name = "tabPageTrims";
            this.tabPageTrims.ShowCloseButton = true;
            this.tabPageTrims.Size = new System.Drawing.Size(1092, 449);
            this.tabPageTrims.TabIndex = 3;
            this.tabPageTrims.Text = "Trims";
            this.tabPageTrims.ThemesEnabled = false;
            // 
            // ChckTrimsRefresh
            // 
            this.ChckTrimsRefresh.AutoSize = true;
            this.ChckTrimsRefresh.Location = new System.Drawing.Point(838, 164);
            this.ChckTrimsRefresh.Name = "ChckTrimsRefresh";
            this.ChckTrimsRefresh.Size = new System.Drawing.Size(111, 22);
            this.ChckTrimsRefresh.TabIndex = 4;
            this.ChckTrimsRefresh.Text = "Trims Refresh";
            this.ChckTrimsRefresh.UseVisualStyleBackColor = true;
            this.ChckTrimsRefresh.CheckedChanged += new System.EventHandler(this.ChckTrimsRefresh_CheckedChanged);
            // 
            // DataGridTrims
            // 
            this.DataGridTrims.AllowUserToAddRows = false;
            this.DataGridTrims.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTrims.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrims.Location = new System.Drawing.Point(14, 19);
            this.DataGridTrims.Name = "DataGridTrims";
            this.DataGridTrims.RowHeadersVisible = false;
            this.DataGridTrims.Size = new System.Drawing.Size(814, 405);
            this.DataGridTrims.TabIndex = 3;
            this.DataGridTrims.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrims_CellValueChanged);
            this.DataGridTrims.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridTrims_DataError);
            // 
            // tabPageTrimsProcess
            // 
            this.tabPageTrimsProcess.Controls.Add(this.groupBox1);
            this.tabPageTrimsProcess.Image = null;
            this.tabPageTrimsProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageTrimsProcess.Location = new System.Drawing.Point(1, 30);
            this.tabPageTrimsProcess.Name = "tabPageTrimsProcess";
            this.tabPageTrimsProcess.ShowCloseButton = true;
            this.tabPageTrimsProcess.Size = new System.Drawing.Size(1092, 449);
            this.tabPageTrimsProcess.TabIndex = 5;
            this.tabPageTrimsProcess.Text = "Trims Process";
            this.tabPageTrimsProcess.ThemesEnabled = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ChckTrimsProcessRefresh);
            this.groupBox1.Controls.Add(this.DataGridTrimsProcess);
            this.groupBox1.Location = new System.Drawing.Point(8, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1076, 439);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // ChckTrimsProcessRefresh
            // 
            this.ChckTrimsProcessRefresh.AutoSize = true;
            this.ChckTrimsProcessRefresh.Location = new System.Drawing.Point(891, 76);
            this.ChckTrimsProcessRefresh.Name = "ChckTrimsProcessRefresh";
            this.ChckTrimsProcessRefresh.Size = new System.Drawing.Size(161, 22);
            this.ChckTrimsProcessRefresh.TabIndex = 3;
            this.ChckTrimsProcessRefresh.Text = "Trims Process Refresh";
            this.ChckTrimsProcessRefresh.UseVisualStyleBackColor = true;
            this.ChckTrimsProcessRefresh.CheckedChanged += new System.EventHandler(this.ChckTrimsProcessRefresh_CheckedChanged);
            // 
            // DataGridTrimsProcess
            // 
            this.DataGridTrimsProcess.AllowUserToAddRows = false;
            this.DataGridTrimsProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridTrimsProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridTrimsProcess.Location = new System.Drawing.Point(6, 25);
            this.DataGridTrimsProcess.Name = "DataGridTrimsProcess";
            this.DataGridTrimsProcess.RowHeadersVisible = false;
            this.DataGridTrimsProcess.Size = new System.Drawing.Size(879, 408);
            this.DataGridTrimsProcess.TabIndex = 5;
            this.DataGridTrimsProcess.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridTrimsProcess_CellMouseDoubleClick);
            this.DataGridTrimsProcess.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTrimsProcess_CellValueChanged);
            // 
            // tabPageGarmentProcess
            // 
            this.tabPageGarmentProcess.Controls.Add(this.groupBox2);
            this.tabPageGarmentProcess.Image = null;
            this.tabPageGarmentProcess.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageGarmentProcess.Location = new System.Drawing.Point(1, 30);
            this.tabPageGarmentProcess.Name = "tabPageGarmentProcess";
            this.tabPageGarmentProcess.ShowCloseButton = true;
            this.tabPageGarmentProcess.Size = new System.Drawing.Size(1092, 449);
            this.tabPageGarmentProcess.TabIndex = 6;
            this.tabPageGarmentProcess.Text = "Garment Process";
            this.tabPageGarmentProcess.ThemesEnabled = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ChckGarmenrRefresh);
            this.groupBox2.Controls.Add(this.DataGridGarmentProcess);
            this.groupBox2.Location = new System.Drawing.Point(8, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1076, 439);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // ChckGarmenrRefresh
            // 
            this.ChckGarmenrRefresh.AutoSize = true;
            this.ChckGarmenrRefresh.Location = new System.Drawing.Point(941, 51);
            this.ChckGarmenrRefresh.Name = "ChckGarmenrRefresh";
            this.ChckGarmenrRefresh.Size = new System.Drawing.Size(132, 22);
            this.ChckGarmenrRefresh.TabIndex = 3;
            this.ChckGarmenrRefresh.Text = "Garment Refresh";
            this.ChckGarmenrRefresh.UseVisualStyleBackColor = true;
            this.ChckGarmenrRefresh.CheckedChanged += new System.EventHandler(this.ChckGarmenrRefresh_CheckedChanged);
            // 
            // DataGridGarmentProcess
            // 
            this.DataGridGarmentProcess.AllowUserToAddRows = false;
            this.DataGridGarmentProcess.BackgroundColor = System.Drawing.Color.White;
            this.DataGridGarmentProcess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridGarmentProcess.Location = new System.Drawing.Point(6, 25);
            this.DataGridGarmentProcess.Name = "DataGridGarmentProcess";
            this.DataGridGarmentProcess.RowHeadersVisible = false;
            this.DataGridGarmentProcess.Size = new System.Drawing.Size(929, 408);
            this.DataGridGarmentProcess.TabIndex = 5;
            this.DataGridGarmentProcess.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridGarmentProcess_CellValueChanged);
            // 
            // tabPageCMT
            // 
            this.tabPageCMT.Controls.Add(this.groupBox3);
            this.tabPageCMT.Image = null;
            this.tabPageCMT.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageCMT.Location = new System.Drawing.Point(1, 30);
            this.tabPageCMT.Name = "tabPageCMT";
            this.tabPageCMT.ShowCloseButton = true;
            this.tabPageCMT.Size = new System.Drawing.Size(1092, 449);
            this.tabPageCMT.TabIndex = 7;
            this.tabPageCMT.Text = "CMT Process";
            this.tabPageCMT.ThemesEnabled = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BtnCMTOK);
            this.groupBox3.Controls.Add(this.txtCMTRate);
            this.groupBox3.Controls.Add(this.txtQty);
            this.groupBox3.Controls.Add(this.CmbOperations);
            this.groupBox3.Controls.Add(this.CmbCOORDINATES);
            this.groupBox3.Controls.Add(this.DataGridCMT);
            this.groupBox3.Location = new System.Drawing.Point(8, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1076, 439);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // BtnCMTOK
            // 
            this.BtnCMTOK.Location = new System.Drawing.Point(594, 26);
            this.BtnCMTOK.Name = "BtnCMTOK";
            this.BtnCMTOK.Size = new System.Drawing.Size(52, 29);
            this.BtnCMTOK.TabIndex = 10;
            this.BtnCMTOK.Text = "Ok";
            this.BtnCMTOK.UseVisualStyleBackColor = true;
            this.BtnCMTOK.Click += new System.EventHandler(this.BtnCMTOK_Click);
            // 
            // txtCMTRate
            // 
            this.txtCMTRate.Location = new System.Drawing.Point(494, 27);
            this.txtCMTRate.Name = "txtCMTRate";
            this.txtCMTRate.Size = new System.Drawing.Size(100, 26);
            this.txtCMTRate.TabIndex = 9;
            this.txtCMTRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtCMTRate_KeyDown);
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(392, 27);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(100, 26);
            this.txtQty.TabIndex = 8;
            // 
            // CmbOperations
            // 
            this.CmbOperations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbOperations.FormattingEnabled = true;
            this.CmbOperations.Location = new System.Drawing.Point(199, 27);
            this.CmbOperations.Name = "CmbOperations";
            this.CmbOperations.Size = new System.Drawing.Size(191, 26);
            this.CmbOperations.TabIndex = 7;
            this.CmbOperations.SelectedIndexChanged += new System.EventHandler(this.CmbOperations_SelectedIndexChanged);
            // 
            // CmbCOORDINATES
            // 
            this.CmbCOORDINATES.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbCOORDINATES.FormattingEnabled = true;
            this.CmbCOORDINATES.Location = new System.Drawing.Point(6, 27);
            this.CmbCOORDINATES.Name = "CmbCOORDINATES";
            this.CmbCOORDINATES.Size = new System.Drawing.Size(191, 26);
            this.CmbCOORDINATES.TabIndex = 6;
            // 
            // DataGridCMT
            // 
            this.DataGridCMT.AllowUserToAddRows = false;
            this.DataGridCMT.BackgroundColor = System.Drawing.Color.White;
            this.DataGridCMT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCMT.Location = new System.Drawing.Point(6, 59);
            this.DataGridCMT.Name = "DataGridCMT";
            this.DataGridCMT.RowHeadersVisible = false;
            this.DataGridCMT.Size = new System.Drawing.Size(876, 348);
            this.DataGridCMT.TabIndex = 5;
            this.DataGridCMT.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCMT_CellValueChanged);
            this.DataGridCMT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridCMT_KeyDown);
            // 
            // tabPageOthers
            // 
            this.tabPageOthers.Controls.Add(this.grOtherExp);
            this.tabPageOthers.Image = null;
            this.tabPageOthers.ImageSize = new System.Drawing.Size(16, 16);
            this.tabPageOthers.Location = new System.Drawing.Point(1, 30);
            this.tabPageOthers.Name = "tabPageOthers";
            this.tabPageOthers.ShowCloseButton = true;
            this.tabPageOthers.Size = new System.Drawing.Size(1092, 449);
            this.tabPageOthers.TabIndex = 4;
            this.tabPageOthers.Text = "Oher Expenses";
            this.tabPageOthers.ThemesEnabled = false;
            // 
            // grOtherExp
            // 
            this.grOtherExp.Controls.Add(this.BtnExpensesOk);
            this.grOtherExp.Controls.Add(this.lblRate);
            this.grOtherExp.Controls.Add(this.txtRate);
            this.grOtherExp.Controls.Add(this.label11);
            this.grOtherExp.Controls.Add(this.CmbType);
            this.grOtherExp.Controls.Add(this.label10);
            this.grOtherExp.Controls.Add(this.CmbOtherExpHead);
            this.grOtherExp.Controls.Add(this.DataGridOtherExpenses);
            this.grOtherExp.Controls.Add(this.lblPercentage);
            this.grOtherExp.Controls.Add(this.txtPercentage);
            this.grOtherExp.Location = new System.Drawing.Point(5, 4);
            this.grOtherExp.Name = "grOtherExp";
            this.grOtherExp.Size = new System.Drawing.Size(1078, 431);
            this.grOtherExp.TabIndex = 0;
            this.grOtherExp.TabStop = false;
            // 
            // BtnExpensesOk
            // 
            this.BtnExpensesOk.Location = new System.Drawing.Point(634, 36);
            this.BtnExpensesOk.Name = "BtnExpensesOk";
            this.BtnExpensesOk.Size = new System.Drawing.Size(43, 28);
            this.BtnExpensesOk.TabIndex = 414;
            this.BtnExpensesOk.Text = "OK";
            this.BtnExpensesOk.UseVisualStyleBackColor = true;
            this.BtnExpensesOk.Click += new System.EventHandler(this.BtnExpensesOk_Click);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRate.Location = new System.Drawing.Point(547, 16);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(31, 15);
            this.lblRate.TabIndex = 413;
            this.lblRate.Text = "Rate";
            this.lblRate.Visible = false;
            // 
            // txtRate
            // 
            this.txtRate.Location = new System.Drawing.Point(534, 38);
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(100, 26);
            this.txtRate.TabIndex = 412;
            this.txtRate.WordWrap = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(395, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 15);
            this.label11.TabIndex = 409;
            this.label11.Text = "Type";
            // 
            // CmbType
            // 
            this.CmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbType.FormattingEnabled = true;
            this.CmbType.Items.AddRange(new object[] {
            "Flat Rate",
            "Pcs Rate",
            "Percentage"});
            this.CmbType.Location = new System.Drawing.Point(392, 37);
            this.CmbType.Name = "CmbType";
            this.CmbType.Size = new System.Drawing.Size(141, 26);
            this.CmbType.TabIndex = 408;
            this.CmbType.SelectedIndexChanged += new System.EventHandler(this.CmbType_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 15);
            this.label10.TabIndex = 407;
            this.label10.Text = "Accounts";
            // 
            // CmbOtherExpHead
            // 
            this.CmbOtherExpHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbOtherExpHead.FormattingEnabled = true;
            this.CmbOtherExpHead.Location = new System.Drawing.Point(9, 37);
            this.CmbOtherExpHead.Name = "CmbOtherExpHead";
            this.CmbOtherExpHead.Size = new System.Drawing.Size(381, 26);
            this.CmbOtherExpHead.TabIndex = 5;
            this.CmbOtherExpHead.SelectedIndexChanged += new System.EventHandler(this.CmbOtherExpHead_SelectedIndexChanged);
            // 
            // DataGridOtherExpenses
            // 
            this.DataGridOtherExpenses.AllowUserToAddRows = false;
            this.DataGridOtherExpenses.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOtherExpenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOtherExpenses.Location = new System.Drawing.Point(9, 65);
            this.DataGridOtherExpenses.Name = "DataGridOtherExpenses";
            this.DataGridOtherExpenses.RowHeadersVisible = false;
            this.DataGridOtherExpenses.Size = new System.Drawing.Size(814, 357);
            this.DataGridOtherExpenses.TabIndex = 4;
            this.DataGridOtherExpenses.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridOtherExpenses_CellValueChanged);
            this.DataGridOtherExpenses.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridOtherExpenses_DataError);
            this.DataGridOtherExpenses.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridOtherExpenses_KeyDown);
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercentage.Location = new System.Drawing.Point(538, 16);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(67, 15);
            this.lblPercentage.TabIndex = 411;
            this.lblPercentage.Text = "Percentage";
            this.lblPercentage.Visible = false;
            // 
            // txtPercentage
            // 
            this.txtPercentage.Location = new System.Drawing.Point(534, 37);
            this.txtPercentage.Name = "txtPercentage";
            this.txtPercentage.Size = new System.Drawing.Size(100, 26);
            this.txtPercentage.TabIndex = 410;
            this.txtPercentage.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(593, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 404;
            this.label3.Text = "Style";
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.BtnPrintgarment);
            this.GrFront.Controls.Add(this.BtnDelete);
            this.GrFront.Controls.Add(this.BtnPrint);
            this.GrFront.Controls.Add(this.SplitAdd);
            this.GrFront.Controls.Add(this.SfDataGridPreBudjet);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(9, 5);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1108, 577);
            this.GrFront.TabIndex = 407;
            this.GrFront.TabStop = false;
            // 
            // BtnPrintgarment
            // 
            this.BtnPrintgarment.Location = new System.Drawing.Point(105, 534);
            this.BtnPrintgarment.Name = "BtnPrintgarment";
            this.BtnPrintgarment.Size = new System.Drawing.Size(122, 35);
            this.BtnPrintgarment.TabIndex = 4;
            this.BtnPrintgarment.Text = "Print GARMENT";
            this.BtnPrintgarment.UseVisualStyleBackColor = true;
            this.BtnPrintgarment.Click += new System.EventHandler(this.BtnPrintgarment_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(266, 534);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(92, 35);
            this.BtnDelete.TabIndex = 3;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Location = new System.Drawing.Point(7, 534);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(92, 35);
            this.BtnPrint.TabIndex = 2;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.UseVisualStyleBackColor = true;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // SfDataGridPreBudjet
            // 
            this.SfDataGridPreBudjet.AccessibleName = "Table";
            this.SfDataGridPreBudjet.AllowDraggingColumns = true;
            this.SfDataGridPreBudjet.AllowFiltering = true;
            this.SfDataGridPreBudjet.Location = new System.Drawing.Point(7, 17);
            this.SfDataGridPreBudjet.Name = "SfDataGridPreBudjet";
            this.SfDataGridPreBudjet.Size = new System.Drawing.Size(1094, 512);
            this.SfDataGridPreBudjet.TabIndex = 0;
            this.SfDataGridPreBudjet.Text = "sfDataGrid1";
            this.SfDataGridPreBudjet.QueryCellStyle += new Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventHandler(this.SfDataGridPreBudjet_QueryCellStyle);
            // 
            // FrmPreBudget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1126, 589);
            this.Controls.Add(this.GrFront);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmPreBudget";
            this.Text = "FrmPreBudget";
            this.Load += new System.EventHandler(this.FrmPreBudget_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControlBudjet)).EndInit();
            this.TabControlBudjet.ResumeLayout(false);
            this.tabPageYarn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabContronYan)).EndInit();
            this.tabContronYan.ResumeLayout(false);
            this.tabPageYarnPurchase.ResumeLayout(false);
            this.tabPageYarnPurchase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnPurchase)).EndInit();
            this.tabPageYanProcess.ResumeLayout(false);
            this.tabPageYanProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridYarnProcess)).EndInit();
            this.tabPageFabric.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlFabric)).EndInit();
            this.tabControlFabric.ResumeLayout(false);
            this.tabPageFabricProcess.ResumeLayout(false);
            this.tabPageFabricProcess.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricProcess)).EndInit();
            this.tabPageFabricPurchase.ResumeLayout(false);
            this.tabPageFabricPurchase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridFabricPurchase)).EndInit();
            this.tabPageTrims.ResumeLayout(false);
            this.tabPageTrims.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrims)).EndInit();
            this.tabPageTrimsProcess.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridTrimsProcess)).EndInit();
            this.tabPageGarmentProcess.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGarmentProcess)).EndInit();
            this.tabPageCMT.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCMT)).EndInit();
            this.tabPageOthers.ResumeLayout(false);
            this.grOtherExp.ResumeLayout(false);
            this.grOtherExp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOtherExpenses)).EndInit();
            this.GrFront.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SfDataGridPreBudjet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.GroupBox grSearch;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDocDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbStyle;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitSave;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv TabControlBudjet;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripBack;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageYarn;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabric;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabContronYan;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageYarnPurchase;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageYanProcess;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlFabric;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabricPurchase;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageFabricProcess;
        private System.Windows.Forms.GroupBox GrFront;
        private Syncfusion.WinForms.DataGrid.SfDataGrid SfDataGridPreBudjet;
        private Syncfusion.Windows.Forms.Tools.SplitButton SplitAdd;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripEdit;
        private Syncfusion.Windows.Forms.Tools.toolstripitem toolstripClose;
        private System.Windows.Forms.DataGridView DataGridYarnPurchase;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrims;
        private System.Windows.Forms.DataGridView DataGridYarnProcess;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtYarnPurchaseQty;
        private System.Windows.Forms.ComboBox CmbYarn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPurchaseRate;
        private System.Windows.Forms.Button BtnYarnProcessOk;
        private System.Windows.Forms.DataGridView DataGridFabricPurchase;
        private System.Windows.Forms.DataGridView DataGridFabricProcess;
        private System.Windows.Forms.ComboBox CmbFabricPurchase;
        private System.Windows.Forms.Button BtnFabricOk;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFabricAmount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFabricQty;
        private System.Windows.Forms.DataGridView DataGridTrims;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageOthers;
        private System.Windows.Forms.GroupBox grOtherExp;
        private System.Windows.Forms.DataGridView DataGridOtherExpenses;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CmbOtherExpHead;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.TextBox txtPercentage;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CmbType;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Button BtnExpensesOk;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrimsProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageGarmentProcess;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageCMT;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DataGridTrimsProcess;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView DataGridGarmentProcess;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView DataGridCMT;
        private System.Windows.Forms.Button BtnCMTOK;
        private System.Windows.Forms.TextBox txtCMTRate;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.ComboBox CmbOperations;
        private System.Windows.Forms.ComboBox CmbCOORDINATES;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.TextBox txtExrate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPcrate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtGrossValue;
        private System.Windows.Forms.CheckBox ChckFabricRefresh;
        private System.Windows.Forms.CheckBox ChckTrimsRefresh;
        private System.Windows.Forms.CheckBox ChckTrimsProcessRefresh;
        private System.Windows.Forms.CheckBox ChckGarmenrRefresh;
        private System.Windows.Forms.CheckBox ChckYarnRefresh;
        private System.Windows.Forms.Button BtnPrintgarment;
        private System.Windows.Forms.CheckBox ChckYarnProcessRefresh;
    }
}