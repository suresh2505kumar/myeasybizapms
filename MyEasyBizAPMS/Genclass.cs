﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace MyEasyBizAPMS
{
    class Genclass
    {
        public static string CompanyName = string.Empty;
        public static string Barcode = string.Empty;
        public static string SortNo = string.Empty;
        public static string ReportType = string.Empty;
        public static DateTime ReortDate = new DateTime();
        public static int RpeortId = 0;
        public static string parameter = string.Empty;
        public static string parameter1 = string.Empty;
        public static Control Parent;

        public static Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        public static void buttonstyleform(Form Frmname)
        {
            foreach (Control ct in Frmname.Controls)
            {
                if (ct is Button)
                {
                    ct.TabStop = false;
                    (ct as Button).FlatStyle = FlatStyle.Flat;
                    (ct as Button).FlatAppearance.BorderSize = 0;
                    (ct as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }

        public static void buttonstylepanel(Panel whatfldone)
        {
            Parent = whatfldone;
            foreach (Control bt in Parent.Controls)
            {
                if (bt is Button)
                {
                    bt.TabStop = false;
                    (bt as Button).FlatStyle = FlatStyle.Flat;
                    (bt as Button).FlatAppearance.BorderSize = 0;
                    (bt as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }
    }
}
